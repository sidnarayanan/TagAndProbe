#include "CEffZFitter.hh"
//#include "CEffPlotter.hh"
#include "KStyle.hh"

#include <iostream>
#include <string>
#include <cstdlib>

int main(int argc, char **argv)
{
  //--------------------------------------------------------------------------------------------------------------
  // Settings and constants
  //==============================================================================================================

  // handle input arguments
  const std::string conf     = argv[1];         // input configuration file
  const int         sigpass  = atoi(argv[2]);	// signal model for PASS sample
  const int         bkgpass  = atoi(argv[3]);	// background model for PASS sample
  const int         sigfail  = atoi(argv[4]);	// signal model for FAIL sample
  const int         bkgfail  = atoi(argv[5]);	// background model for FAIL sample
  const std::string infname  = argv[6];         // input ROOT file of probes
  const std::string outdir   = argv[7];         // output directory
  const int         doPU     = atoi(argv[8]);	// PU re-weighting mode
  const int         charge   = atoi(argv[9]);	// probe charge requirement (0, -1, +1)
  const std::string temfname = argv[10];        // ROOT file for generating MC-based templates
  const std::string refDir = argv[11];        // reference dir for fixed parameters
  const std::string weightname = (argc>12) ? argv[12] : "scale1fb";
  const bool        doSmearing = (argc>13) ? (atoi(argv[13])!=0) : false;
  
  // other settings
  const double       massLo    = 50;
  const double       massHi    = 300;
  const double       fitMassLo = 50;
  const double       fitMassHi = 300;
  const int          uncMethod = 0;
  const std::string  pufname   = doPU ? "PUWeights_2012.root" : "none";
  const unsigned int runNumLo  = 0;
  const unsigned int runNumHi  = 99999999;
  
  std::cout << std::endl;
  std::cout << " <> Using binning configuration: " << conf << std::endl;
  std::cout << " <> Processing probes file: " << infname << std::endl;
  std::cout << std::endl;

  KStyle();  
  
  //--------------------------------------------------------------------------------------------------------------
  // Measure efficiency using Z events
  //==============================================================================================================
/**************************************************************************
  CEffZFitter Overview:
  initialize()
    ==> parseConf()
    ==> set up output dirs, PU weights, pass/fail trees, templates
    ==> makeBinnedTemplates() / makeUnbinnedTemplates()
  computeEff()
    ==> makeEffGraph()
      ==> loop over bins
      ==> performCount()
      ==> parseFitResults()
      ==> performFit()
        ==> printCorrelations
    ==> makeEffHist2D()
      ==> loop over bins
      ==> performCount()
      ==> parseFitResults()
      ==> performFit()
        ==> printCorrelations
    ==> produce output
***************************************************************************/
  CEffZFitter fitter;
  fitter.initialize(conf, sigpass, bkgpass, sigfail, bkgfail,
                    infname, outdir, temfname, refDir, 
                    massLo, massHi, fitMassLo, fitMassHi, 
		    uncMethod, pufname, charge, runNumLo, runNumHi, weightname, doSmearing);
//  fitter.massCutLo = 110; fitter.massCutHi = 210;
  fitter.computeEff();
  

  std::cout << " <> Output saved in " << outdir << std::endl;

  return 0;
}
