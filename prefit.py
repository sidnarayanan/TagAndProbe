#!/usr/bin/env python

from ROOT import TFile, TF1, TCanvas

def printVar(name,central):
#  if name.find('frac')>=0:
#    print '%s = new RooRealVar(vname,vname,%f,%f,%f);'%(name,central,0,1)
#  else:
    #print '%s = new RooRealVar(vname,vname,%f,%f,%f);'%(name,central,central*0.8,central*1.2)
    print '%s = new RooRealVar(vname,vname,%f,%f,%f);'%(name,central,central,central)

def fit(filename,histname,funcType):
  fin = TFile(filename)
  hist = fin.Get(histname)
  if funcType==0:
    func = TF1("test","gaus(0)+gaus(3)+landau(6)",50,300)
    for iP in xrange(9):
      func.SetParameter(iP,10)
    hist.Fit(func,"Q")

    norm0 = func.GetParameter(0)
    norm1 = func.GetParameter(3)
    norm2 = func.GetParameter(6)

    intTotal = func.Integral(50,300)
    func.SetParameter(3,0); func.SetParameter(6,0); int0 = func.Integral(50,300)    # 1 and 2 are 0
    func.SetParameter(0,0); func.SetParameter(3,norm1); int1 = func.Integral(50,300) # 0 and 2 are 0
    func.SetParameter(3,0); func.SetParameter(6,norm2); int2 = func.Integral(50,300) # 0 and 1 are 0
    
    func.SetParameter(0,norm0); func.SetParameter(3,norm1)

    frac0 = min(1,max(0,int0/intTotal))
    frac1 = min(1,max(0,int1/intTotal))

    print
    printVar("frac0",frac0)
    printVar("frac1",frac1)
    printVar("gaussMu0",func.GetParameter(1))
    printVar("gaussMu1",func.GetParameter(4))
    printVar("landauMu",func.GetParameter(7))
    printVar("gaussSigma0",func.GetParameter(2))
    printVar("gaussSigma1",func.GetParameter(5))
    printVar("landauSigma",func.GetParameter(8))
    print

    c = TCanvas()
    hist.Draw("hist")
    func.Draw("same")
    c.SaveAs("~/public_html/figs/monotop/tnp/hf/ggl_%s.png"%histname)
  if funcType==1:
    func = TF1("test","expo(0)+gaus(2)",50,300)
    for iP in xrange(5):
      func.SetParameter(iP,10)
    func.SetParameter(0,0)
    func.SetParameter(1,-.01)
    if histname.find("pass")>=0:
      func.SetParameter(3,177)
    else:
      func.SetParameter(3,80)
    hist.Fit(func,"")
    
    norm = func.GetParameter(0) + func.GetParameter(2)
    frac = func.GetParameter(0)/norm
   
    norm0 = func.GetParameter(0)
    norm1 = func.GetParameter(2)

    intTotal = func.Integral(50,300)
    func.SetParameter(2,0); int0 = func.Integral(50,300)    # 1 is 0
    func.SetParameter(0,-9999); func.SetParameter(2,norm1); int1 = func.Integral(50,300) # 0 and 2 are 0
    
    func.SetParameter(0,norm0); func.SetParameter(2,norm1)

    frac = int0/intTotal
    
    print
    printVar("frac",frac)
    printVar("alpha",func.GetParameter(1))
    printVar("mu",func.GetParameter(3))
    printVar("sigma",func.GetParameter(4))
    print

    c = TCanvas()
    hist.Draw("hist")
    func.Draw("same")
    c.SaveAs("~/public_html/figs/monotop/tnp/hf/eg_%s.png"%histname)
  if funcType==2:
    func = TF1("test","landau(0)+expo(3)",50,300)
    for iP in xrange(9):
      func.SetParameter(iP,1)
    func.SetParameter(4,-1)
    hist.Fit(func,"Q")
    c = TCanvas()
    hist.Draw("hist")
    func.Draw("same")
    c.SaveAs("~/public_html/figs/monotop/tnp/hf/el_%s.png"%histname)


fit("sig_binnedTemplates.root","passpt_0",0)
fit("sig_binnedTemplates.root","failpt_0",0)

fit("bkg_binnedTemplates.root","passpt_0",1)
fit("bkg_binnedTemplates.root","failpt_0",1)
#fit("bkg_binnedTemplates.root","failpt_0",2)
