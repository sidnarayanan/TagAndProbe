#include "TROOT.h"
#include "TH1D.h"
#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooAbsPdf.h"
#include "RooAddPdf.h"
#include "RooArgList.h"
#include "RooBreitWigner.h"
#include "RooCBShape.h"
#include "RooGaussian.h"
#include "RooFFTConvPdf.h"
#include "RooDataHist.h"
#include "RooHistPdf.h"
#include "RooVoigtianShape.h"
#include "RooVoigtian.h"
#include "RooKeysPdf.h"
#include "RooExponential.h"
#include "RooLandau.h"
#include <algorithm>

#define SMEAR 0

class CSignalModel
{
public:
  CSignalModel():model(0){}
  virtual ~CSignalModel(){ delete model; model=0; }
  RooAbsPdf *model;
};

class CBreitWignerConvCrystalBall : public CSignalModel
{
public:
  CBreitWignerConvCrystalBall(RooRealVar &m, const Bool_t pass);
  ~CBreitWignerConvCrystalBall();
  RooRealVar     *mass, *width;
  RooBreitWigner *bw;
  RooRealVar     *mean, *sigma, *alpha, *n;
  RooCBShape     *cb;
};

class CBWCBPlusVoigt : public CSignalModel
{
public:
  CBWCBPlusVoigt(RooRealVar &m, const Bool_t pass, double ptMin, double ptMax);
  ~CBWCBPlusVoigt();
  RooRealVar     *mass, *width;
  RooBreitWigner *bw;
  RooRealVar     *mean, *sigma, *alpha, *n, *vMean, *vWidth, *vSigma, *fsrFrac;
  RooFormulaVar  *oneMinusFsrFrac;
  RooCBShape     *cb;
  RooVoigtian    *voigt;
  RooAbsPdf      *bwcb;
};

class CMCTemplateConvGaussian : public CSignalModel
{
public:
  CMCTemplateConvGaussian(RooRealVar &m, TH1D* hist, const Bool_t pass, RooRealVar *sigma0=0, int intOrder=1, bool doConv=false);
  ~CMCTemplateConvGaussian();
  RooRealVar  *mean, *sigma;
  RooGaussian *gaus;
  TH1D        *inHist;
  RooDataHist *dataHist;
  RooHistPdf  *histPdf;
};

class CVoigtianCBShape : public CSignalModel
{
public:
  CVoigtianCBShape(RooRealVar &m, const Bool_t pass);
  ~CVoigtianCBShape();
  RooRealVar *mass, *width;
  RooRealVar *sigma, *alpha, *n;
};

class CMCDatasetConvGaussian : public CSignalModel
{
public:
  CMCDatasetConvGaussian(RooRealVar &m, TTree* tree, const Bool_t pass, RooRealVar *sigma0=0);
  ~CMCDatasetConvGaussian();
  RooRealVar  *mean, *sigma;
  RooGaussian *gaus;
  TTree       *inTree;
  RooDataSet  *dataSet;
  RooKeysPdf  *keysPdf;
};

class CNGauss : public CSignalModel
{
  // kill me now
  public:
    CNGauss(RooRealVar &m, const Bool_t pass, int nPeaks, double mu0_init, double frac0_cent=-1, double mu1_init=0, double frac1_cent = -1, double mu2_init=0);
    ~CNGauss();
    RooRealVar *mu0=0, *mu1=0, *mu2=0;
    RooRealVar *sigma0=0, *sigma1=0, *sigma2=0;
    RooRealVar *frac0=0, *frac1=0;
    RooBreitWigner *gaus0=0, *gaus1=0, *gaus2=0;
};

class CSigGauss : public CSignalModel
{
  public:
    CSigGauss(RooRealVar &m, const Bool_t pass, double mu0_init, double mu1_init);
    ~CSigGauss();
    RooRealVar *mu0, *mu1;
    RooRealVar *sigma0, *sigma1;
    RooRealVar *frac0, *frac1;
    RooGaussian *gaus0, *gaus1;
};

class CGGL : public CSignalModel
{
  // two gaussians plus a landau
  public:
    CGGL(RooRealVar &m, const Bool_t pass);
    ~CGGL();
    RooRealVar *gaussMu0, *gaussMu1, *landauMu;
    RooRealVar *gaussSigma0, *gaussSigma1, *landauSigma;
    RooRealVar *frac0, *frac1;
    RooGaussian *gaus0, *gaus1;
    RooLandau *landau;
};

//--------------------------------------------------------------------------------------------------
CGGL::CGGL(RooRealVar &m, const Bool_t pass)
{
  char name[10];
  if(pass) sprintf(name,"%s","Pass");
  else     sprintf(name,"%s","Fail");
  
  char vname[50];
  
  if (pass) {
    sprintf(vname,"gaussMu0%s",name);
gaussMu0 = new RooRealVar(vname,vname,175.199680,175.199680,175.199680);
//    gaussMu0 = new RooRealVar(vname,vname,175.199680,140.159744,210.239616);    
    sprintf(vname,"gaussSigma0%s",name);
gaussSigma0 = new RooRealVar(vname,vname,19.918788,19.918788,19.918788);
//    gaussSigma0 = new RooRealVar(vname,vname,19.918788,15.935030,23.902545);    
    
    sprintf(vname,"gaussMu1%s",name);
gaussMu1 = new RooRealVar(vname,vname,98.313290,98.313290,98.313290);
//    gaussMu1 = new RooRealVar(vname,vname,98.313290,78.650632,117.975948); 
    sprintf(vname,"gaussSigma1%s",name);
gaussSigma1 = new RooRealVar(vname,vname,25.829415,25.829415,25.829415);
//    gaussSigma1 = new RooRealVar(vname,vname,25.829415,20.663532,30.995298);

    sprintf(vname,"landauMu%s",name);
landauMu = new RooRealVar(vname,vname,104.331156,104.331156,104.331156);
//    landauMu = new RooRealVar(vname,vname,104.331156,83.464925,125.197387);
    sprintf(vname,"landauSigma%s",name);
landauSigma = new RooRealVar(vname,vname,26.680615,26.680615,26.680615);
//    landauSigma = new RooRealVar(vname,vname,26.680615,21.344492,32.016738);
    
    sprintf(vname,"frac0%s",name);
    frac0 = new RooRealVar(vname,vname,0.224997,0.224997,0.224997); 
    sprintf(vname,"frac1%s",name);
    frac1 = new RooRealVar(vname,vname,0.2,0.2,0.2); 
  } else {
    sprintf(vname,"gaussMu0%s",name);
//    gaussMu0 = new RooRealVar(vname,vname,80.931089,64.744871,97.117306);
gaussMu0 = new RooRealVar(vname,vname,80.931089,80.931089,80.931089);
    sprintf(vname,"gaussSigma0%s",name);
//    gaussSigma0 = new RooRealVar(vname,vname,8.159225,6.527380,9.791070);
gaussSigma0 = new RooRealVar(vname,vname,8.159225,8.159225,8.159225);
    
    sprintf(vname,"gaussMu1%s",name);
//    gaussMu1 = new RooRealVar(vname,vname,167.017852,133.614282,200.421423);
gaussMu1 = new RooRealVar(vname,vname,167.017852,167.017852,167.017852);
    sprintf(vname,"gaussSigma1%s",name);
//    gaussSigma1 = new RooRealVar(vname,vname,23.358310,18.686648,28.029972);
gaussSigma1 = new RooRealVar(vname,vname,23.358310,23.358310,23.358310);

    sprintf(vname,"landauMu%s",name);
//    landauMu = new RooRealVar(vname,vname,81.459638,65.167710,97.751565);
landauMu = new RooRealVar(vname,vname,81.459638,81.459638,81.459638);
    sprintf(vname,"landauSigma%s",name);
//    landauSigma = new RooRealVar(vname,vname,33.048325,26.438660,39.657989);
landauSigma = new RooRealVar(vname,vname,33.048325,33.048325,33.048325);
    
    sprintf(vname,"frac0%s",name);
    frac0 = new RooRealVar(vname,vname,0.167195,0.167195,0.167195); 
    sprintf(vname,"frac1%s",name);
    frac1 = new RooRealVar(vname,vname,0.179340,0.179340,0.179340);
  }

  sprintf(vname,"gaus0%s",name);
  gaus0 = new RooGaussian(vname,vname,m,*gaussMu0,*gaussSigma0);
  sprintf(vname,"gaus1%s",name);
  gaus1 = new RooGaussian(vname,vname,m,*gaussMu1,*gaussSigma1);
  sprintf(vname,"landau%s",name);
  landau = new RooLandau(vname,vname,m,*landauMu,*landauSigma);

  model = new RooAddPdf(vname,vname,RooArgList(*gaus0,*gaus1,*landau),RooArgList(*frac0,*frac1));

}

CGGL::~CGGL()
{
  delete gaussMu0;     gaussMu0=0;
  delete gaussSigma0;  gaussSigma0=0;
  delete gaussMu1;     gaussMu1=0;
  delete gaussSigma1;  gaussSigma1=0;
  delete landauMu;     landauMu=0;
  delete landauSigma;  landauSigma=0;
  delete frac0;        frac0=0;
  delete frac1;        frac1=0;
  delete gaus0;        gaus0=0;
  delete gaus1;        gaus1=0;
  delete landau;       landau=0;
}

//--------------------------------------------------------------------------------------------------
CSigGauss::CSigGauss(RooRealVar &m, const Bool_t pass,double mu0_init, double mu1_init)
{
  char name[10];
  if(pass) sprintf(name,"%s","Pass");
  else     sprintf(name,"%s","Fail");
  
  char vname[50];
  
  sprintf(vname,"mu0%s",name);
  mu0 = new RooRealVar(vname,vname,mu0_init,mu0_init-50, mu0_init+50);
  sprintf(vname,"sigma0%s",name);
  sigma0 = new RooRealVar(vname,vname,10,0.1,100);    
  sprintf(vname,"gaus0%s",name);
  gaus0 = new RooGaussian(vname,vname,m,*mu0,*sigma0);
  
  sprintf(vname,"mu1%s",name);
  mu1 = new RooRealVar(vname,vname,mu1_init,mu1_init-50, mu1_init+50);
  sprintf(vname,"sigma1%s",name);
  sigma1 = new RooRealVar(vname,vname,10,0.1,100);    
  sprintf(vname,"gaus1%s",name);
  gaus1 = new RooGaussian(vname,vname,m,*mu1,*sigma1);
  sprintf(vname,"frac0%s",name);     
  frac0 = new RooRealVar(vname,vname,.5,0,1);
  sprintf(vname,"frac1%s",name);     
  frac1 = new RooRealVar(vname,vname,.5,0,1);
  sprintf(vname,"signal%s",name);
  model = new RooAddPdf(vname,vname,RooArgList(*gaus0,*gaus1),RooArgList(*frac0,*frac1));

}

CSigGauss::~CSigGauss()
{
  delete mu0;     mu0=0;
  delete sigma0;  sigma0=0;
  delete mu1; mu1=0;
  delete sigma1; sigma1=0;
  delete frac0; frac0=0;
  delete frac1; frac1=0;
  delete gaus0; gaus0=0;
  delete gaus1; gaus1=0;
}


//--------------------------------------------------------------------------------------------------
double shiftCentral(double cent, double shift, bool isUp) {
  if (shift>0.999) return ((isUp) ? 1 : 0);
  else return ((isUp) ? std::min(1.,(1+shift)*cent) : std::max(0.,(1-shift)*cent));
}

CNGauss::CNGauss(RooRealVar &m, const Bool_t pass, int nPeaks, double mu0_init, double frac0_cent, double mu1_init, double frac1_cent, double mu2_init)
{

  char name[10];
  if(pass) sprintf(name,"%s","Pass");
  else     sprintf(name,"%s","Fail");
  
  double fracShift=.2; // let fractions shift by 20%
  if (nPeaks==2 && frac0_cent<0) {
    frac0_cent = 0.5;
    fracShift = 1.;
  } else if (nPeaks==3 && frac1_cent<1) {
    frac0_cent = 0.3;
    frac1_cent = 0.3;
    fracShift = 1.;
  }

  char vname[50];
  sprintf(vname,"mu0%s",name);
  mu0 = new RooRealVar(vname,vname,mu0_init,mu0_init-20, mu0_init+30);

  sprintf(vname,"sigma0%s",name);
  sigma0 = new RooRealVar(vname,vname,100,40,200);    
  
  if (nPeaks==1)
    sprintf(vname,"signal%s",name);
  else
    sprintf(vname,"gaus0%s",name);
  gaus0 = new RooBreitWigner(vname,vname,m,*mu0,*sigma0);
 
  if (nPeaks>1) { 
    sprintf(vname,"frac0%s",name);     
    frac0 = new RooRealVar(vname,vname,
                            frac0_cent,
                            shiftCentral(frac0_cent,fracShift,false),
                            shiftCentral(frac0_cent,fracShift,true));

    sprintf(vname,"mu1%s",name);
    mu1 = new RooRealVar(vname,vname,mu1_init,mu1_init-50, mu1_init+50);
  
    sprintf(vname,"sigma1%s",name);
    sigma1 = new RooRealVar(vname,vname,50,40,100);    
  
    sprintf(vname,"gaus1%s",name);
    gaus1 = new RooBreitWigner(vname,vname,m,*mu1,*sigma1);

    if (nPeaks>2) {
      sprintf(vname,"frac0%s",name);     
      frac1 = new RooRealVar(vname,vname,
                              frac1_cent,
                              shiftCentral(frac1_cent,fracShift,false),
                              shiftCentral(frac1_cent,fracShift,true));

      sprintf(vname,"mu2%s",name);
      mu2 = new RooRealVar(vname,vname,mu2_init,mu2_init-50, mu2_init+50);
    
      sprintf(vname,"sigma2%s",name);
      sigma2 = new RooRealVar(vname,vname,10,1,100);    
    
      sprintf(vname,"gaus2%s",name);
      gaus2 = new RooBreitWigner(vname,vname,m,*mu2,*sigma2);
    }
  }


  sprintf(vname,"signal%s",name);
  if (nPeaks==3)
    model = new RooAddPdf(vname,vname,RooArgList(*gaus0,*gaus1,*gaus2),RooArgList(*frac0,*frac1));
  else if (nPeaks==2)
    model = new RooAddPdf(vname,vname,RooArgList(*gaus0,*gaus1),RooArgList(*frac0));
  else if (nPeaks==1) {
    model = gaus0;
    gaus0=0; // so we don't try to double-free memory
  }
   
}

CNGauss::~CNGauss()
{
  delete mu0;     mu0=0;
  delete mu1;     mu1=0;
  delete mu2;     mu2=0;
  delete sigma0;  sigma0=0;
  delete sigma1;  sigma1=0;
  delete sigma2;  sigma2=0;
  delete frac0;   frac0=0;
  delete frac1;   frac1=0;
  delete gaus0;   gaus0=0;
  delete gaus1;   gaus1=0;
  delete gaus2;   gaus2=0;
}

//--------------------------------------------------------------------------------------------------
CBreitWignerConvCrystalBall::CBreitWignerConvCrystalBall(RooRealVar &m, const Bool_t pass)
{
  char name[10];
  if(pass) sprintf(name,"%s","Pass");
  else     sprintf(name,"%s","Fail");
  
  char vname[50];
  
  sprintf(vname,"mass%s",name);
  mass = new RooRealVar(vname,vname,170,110,210);    
  mass->setVal(91.1876);
  mass->setConstant(kTRUE);
  
  sprintf(vname,"width%s",name);
  width = new RooRealVar(vname,vname,50,0.1,100);    
  width->setVal(2.4952);
  width->setConstant(kTRUE);
  
  sprintf(vname,"bw%s",name);
  bw = new RooBreitWigner(vname,vname,m,*mass,*width);

  if(pass) {
    sprintf(vname,"mean%s",name);  mean  = new RooRealVar(vname,vname,0,-10,10);
    sprintf(vname,"sigma%s",name); sigma = new RooRealVar(vname,vname,1,0.1,5);
    sprintf(vname,"alpha%s",name); alpha = new RooRealVar(vname,vname,5,0,20);
    sprintf(vname,"n%s",name);     n     = new RooRealVar(vname,vname,1,0,10);
  } else {
    sprintf(vname,"mean%s",name);  mean  = new RooRealVar(vname,vname,0,-10,10);
    sprintf(vname,"sigma%s",name); sigma = new RooRealVar(vname,vname,1,0.1,5);
    sprintf(vname,"alpha%s",name); alpha = new RooRealVar(vname,vname,5,0,20);
    sprintf(vname,"n%s",name);     n     = new RooRealVar(vname,vname,1,0,10);
  }  
//  n->setVal(1.0);
//  n->setConstant(kTRUE);
  
  sprintf(vname,"cb%s",name);
  cb = new RooCBShape(vname,vname,m,*mean,*sigma,*alpha,*n);
        
  sprintf(vname,"signal%s",name);
  model = new RooFFTConvPdf(vname,vname,m,*bw,*cb);
}

CBreitWignerConvCrystalBall::~CBreitWignerConvCrystalBall()
{
  delete mass;  mass=0;
  delete width; width=0;
  delete bw;    bw=0;
  delete mean;  mean=0;
  delete sigma; sigma=0;
  delete alpha; alpha=0;
  delete n;     n=0;
  delete cb;    cb=0;
}

//--------------------------------------------------------------------------------------------------
CBWCBPlusVoigt::CBWCBPlusVoigt(RooRealVar &m, const Bool_t pass, double ptMin, double ptMax)
{
  char name[10];
  if(pass) sprintf(name,"%s","Pass");
  else     sprintf(name,"%s","Fail");
  
  char vname[50];
  char formula[100];
  
  sprintf(vname,"mass%s",name);
  mass = new RooRealVar(vname,vname,91,80,100);    
  mass->setVal(91.1876);
  mass->setConstant(kTRUE);
  
  sprintf(vname,"width%s",name);
  width = new RooRealVar(vname,vname,2.5,0.1,10);    
  width->setVal(2.4952);
  width->setConstant(kTRUE);
  
  sprintf(vname,"bw%s",name);
  bw = new RooBreitWigner(vname,vname,m,*mass,*width);
  
  bool electrons=false;
  double fsrPeak=80;
  double fsrSigma=6;
  double fsrSigmaMin=3;
  double fsrPeakRange=15;
  double fsrFracMin=0;
  double fsrFracMax=0.3;
  double fsrFracInit=0.1;
  if(electrons) fsrFracInit = 0.3;
  if(electrons) fsrFracMax = 0.8;
  if(ptMin >= 100) {
    fsrPeak=140;
  } else if(ptMin >= 70) {
    fsrPeak=115;
  } else if(ptMin >= 50) {
    fsrPeak=105;
    fsrPeakRange=10;
    if(electrons) fsrFracInit=0.41;
    if(electrons) fsrFracMin=.1;
    fsrSigma=7;
  } else if(ptMin >= 40) {
    fsrPeak=95;
    if(electrons) fsrFracMin=.1;
  } else if(ptMin >= 30) {
    fsrPeak=80;
    if(electrons) fsrFracMin=.1;
    fsrPeakRange=10;
  } else if(ptMin >= 20) {
    fsrPeak=75;
    if(electrons) fsrFracMin=.2;
  } else if(ptMin >= 10) {
    fsrPeak=65;
    if(electrons) fsrFracMin=.2;
    fsrPeakRange=10;
    fsrSigma=10;
    fsrSigmaMin=10;
  } else {
    fsrPeakRange=300;
  }

  //sprintf(vname,"vMean%s",name);      vMean  = new RooRealVar(vname,vname,60,5,150);
  //sprintf(vname,"vMean%s",name);      vMean  = new RooRealVar(vname,vname,fsrPeak,5,150);
  sprintf(vname,"vMean%s",name);      vMean  = new RooRealVar(vname,vname,fsrPeak,fsrPeak-fsrPeakRange,fsrPeak+fsrPeakRange);
  sprintf(vname,"vWidth%s",name);     vWidth = new RooRealVar(vname,vname,0.01,0.001,10);
  sprintf(vname,"vSigma%s",name);     vSigma = new RooRealVar(vname,vname,fsrSigma, fsrSigmaMin,50);
  if(pass) {
    sprintf(vname,"mean%s",name);       mean   = new RooRealVar(vname,vname,0,-10,10);
    sprintf(vname,"sigma%s",name);      sigma  = new RooRealVar(vname,vname,1,0.01,15);
    sprintf(vname,"alpha%s",name);      alpha  = new RooRealVar(vname,vname,5,-20,20);
    sprintf(vname,"n%s",name);          n      = new RooRealVar(vname,vname,1,0,10);
    sprintf(vname,"fsrFrac%s",name);    fsrFrac= new RooRealVar(vname,vname, fsrFracInit, 0,0.8);
  } else {
    sprintf(vname,"mean%s",name);       mean  = new RooRealVar(vname,vname,0,-10,10);
    sprintf(vname,"sigma%s",name);      sigma = new RooRealVar(vname,vname,1,0.01,15);
    sprintf(vname,"alpha%s",name);      alpha = new RooRealVar(vname,vname,5,-20,20);
    sprintf(vname,"n%s",name);          n     = new RooRealVar(vname,vname,1,0,10);
    sprintf(vname,"fsrFrac%s",name);    fsrFrac= new RooRealVar(vname,vname, fsrFracInit, fsrFracMin,fsrFracMax);
  }
  sprintf(formula, "1 - fsrFrac%s", name);
  sprintf(vname,"oneMinusFsrFrac%s",name);    oneMinusFsrFrac= new RooFormulaVar(vname,vname,formula, *fsrFrac);
  //sprintf(vname, "fsrNorm%s", name);    fsrNorm = new RooFormulaVar(vname, "fsrFrac", fsrFrac);
//  n->setVal(1.0);
//  n->setConstant(kTRUE);
  
  sprintf(vname,"cb%s",name);
  cb = new RooCBShape(vname,vname,m,*mean,*sigma,*alpha,*n);
  
  sprintf(vname, "voigt%s",name);
  voigt = new RooVoigtian(vname,vname, m, *vMean, *vWidth, *vSigma);
  sprintf(vname,"bwcb%s",name);
  bwcb = new RooFFTConvPdf(vname,vname,m,*bw,*cb);
  RooArgList *pdfs = new RooArgList(*voigt, *bwcb);
  RooArgList *coeffs = new RooArgList(*fsrFrac, *oneMinusFsrFrac); 
  sprintf(vname,"signal%s",name);
  model = new RooAddPdf(vname, vname, *pdfs, *coeffs);
  //model = new RooFFTConvPdf(vname,vname,m,*bw,*cb);
}

CBWCBPlusVoigt::~CBWCBPlusVoigt()
{
  delete vMean;    vMean=0;
  delete vWidth;    vWidth=0;
  delete vSigma;    vSigma=0;
  delete oneMinusFsrFrac;    oneMinusFsrFrac=0;
  delete fsrFrac;    fsrFrac=0;
  delete voigt; voigt=0;
  delete bwcb;  bwcb=0;
  delete mass;  mass=0;
  delete width; width=0;
  delete bw;    bw=0;
  delete mean;  mean=0;
  delete sigma; sigma=0;
  delete alpha; alpha=0;
  delete n;     n=0;
  delete cb;    cb=0;
}

//--------------------------------------------------------------------------------------------------
CMCTemplateConvGaussian::CMCTemplateConvGaussian(RooRealVar &m, TH1D* hist, const Bool_t pass, RooRealVar *sigma0, int intOrder, bool doConv)
{  
  char name[10];
  if(pass) sprintf(name,"%s","Pass");
  else     sprintf(name,"%s","Fail");
  
  char vname[50];  
  
  if (doConv) {
    printf("TURNING SMEARING ON FOR SIGNAL MODEL %s\n",name);
    sprintf(vname,"mean%s",name);  mean  = new RooRealVar(vname,vname,0.,-10.,10.);
    if(sigma0) { sigma = sigma0; }
    else       { sprintf(vname,"sigma%s",name); sigma = new RooRealVar(vname,vname,.5,.5,10.); }
  } else {
    sprintf(vname,"mean%s",name);  mean  = new RooRealVar(vname,vname,0.,-0.,0.);
    if(sigma0) { sigma = sigma0; }
    else       { sprintf(vname,"sigma%s",name); sigma = new RooRealVar(vname,vname,.5,.5,.5); }
  }
  sprintf(vname,"gaus%s",name);  gaus  = new RooGaussian(vname,vname,m,*mean,*sigma);


  sprintf(vname,"inHist_%s",hist->GetName());
  inHist = (TH1D*)hist->Clone(vname);
  
  sprintf(vname,"dataHist%s",name); dataHist = new RooDataHist(vname,vname,RooArgSet(m),inHist);
  sprintf(vname,"histPdf%s",name);  histPdf  = new RooHistPdf(vname,vname,m,*dataHist,intOrder);
//  histPdf->setInterpolationOrder(10);
  sprintf(vname,"signal%s",name);   model    = new RooFFTConvPdf(vname,vname,m,*histPdf,*gaus);
}

CMCTemplateConvGaussian::~CMCTemplateConvGaussian()
{
  delete mean;     mean=0;
  //delete sigma;    sigma=0;
  delete gaus;     gaus=0;
  delete inHist;   inHist=0;
  delete dataHist; dataHist=0;
  delete histPdf;  histPdf=0;
}

//--------------------------------------------------------------------------------------------------
CVoigtianCBShape::CVoigtianCBShape(RooRealVar &m, const Bool_t pass)
{
  char name[10];
  if(pass) sprintf(name,"%s","Pass");
  else     sprintf(name,"%s","Fail");
  
  char vname[50];
  
  sprintf(vname,"mass%s",name);
  mass = new RooRealVar(vname,vname,91,80,100);
    
  sprintf(vname,"width%s",name);
  width = new RooRealVar(vname,vname,2.5,0.1,10);    
  width->setVal(2.4952);
  width->setConstant(kTRUE);
  
  if(pass) {
    sprintf(vname,"sigma%s",name); sigma = new RooRealVar(vname,vname,1,0.1,10);
    sprintf(vname,"alpha%s",name); alpha = new RooRealVar(vname,vname,5,0,20);
  } else {
    sprintf(vname,"sigma%s",name); sigma = new RooRealVar(vname,vname,1,0.1,10);
    sprintf(vname,"alpha%s",name); alpha = new RooRealVar(vname,vname,5,0,20);
  }
  
  sprintf(vname,"n%s",name);     
  n = new RooRealVar(vname,vname,1,0,10);
  n->setVal(1.0);
  
  sprintf(vname,"signal%s",name);
  model = new RooVoigtianShape(vname,vname,m,*mass,*sigma,*alpha,*n,*width,0);  
}

CVoigtianCBShape::~CVoigtianCBShape()
{
  delete mass;  mass=0;
  delete width; width=0;
  delete sigma; sigma=0;
  delete alpha; alpha=0;
  delete n;     n=0;
}

//--------------------------------------------------------------------------------------------------
CMCDatasetConvGaussian::CMCDatasetConvGaussian(RooRealVar &m, TTree* tree, const Bool_t pass, RooRealVar *sigma0)
{  
  char name[10];
  if(pass) sprintf(name,"%s","Pass");
  else     sprintf(name,"%s","Fail");
  
  char vname[50];  

  if(pass) {
    sprintf(vname,"mean%s",name);  mean  = new RooRealVar(vname,vname,0,-10,10);
    if(sigma0) { sigma = sigma0; }
    else       { sprintf(vname,"sigma%s",name); sigma = new RooRealVar(vname,vname,2,0,5); }
    sprintf(vname,"gaus%s",name);  gaus  = new RooGaussian(vname,vname,m,*mean,*sigma);
  } else {
    sprintf(vname,"mean%s",name);  mean  = new RooRealVar(vname,vname,0,-10,10);
    if(sigma0) { sigma = sigma0; }
    else       { sprintf(vname,"sigma%s",name); sigma = new RooRealVar(vname,vname,2,0,5); }
    sprintf(vname,"gaus%s",name);  gaus  = new RooGaussian(vname,vname,m,*mean,*sigma);
  }
  
  sprintf(vname,"inTree_%s",tree->GetName());
  inTree = (TTree*)tree->Clone(vname);
  
  sprintf(vname,"dataSet%s",name); dataSet = new RooDataSet(vname,vname,inTree,RooArgSet(m));
  sprintf(vname,"keysPdf%s",name); keysPdf = new RooKeysPdf(vname,vname,m,*dataSet,RooKeysPdf::NoMirror,1);
  sprintf(vname,"signal%s",name);  model   = new RooFFTConvPdf(vname,vname,m,*keysPdf,*gaus);
}

CMCDatasetConvGaussian::~CMCDatasetConvGaussian()
{
  delete mean;    mean=0;
  delete sigma;   sigma=0;
  delete gaus;    gaus=0;
  delete inTree;  inTree=0;
  delete dataSet; dataSet=0;
  delete keysPdf; keysPdf=0;
}
