#include "RooAbsPdf.h"
#include "RooAddPdf.h"
#include "RooRealVar.h"
#include "RooExponential.h"
#include "RooCMSShape.h"
#include "RooGenericPdf.h"
#include "RooBreitWigner.h"
#include <vector>

#define SMEARBKG 0


class CBackgroundModel
{
public:
  CBackgroundModel():model(0){}
  virtual ~CBackgroundModel() { delete model; model=0; }
  RooAbsPdf *model;
protected:
  std::vector<double> readBkgParams(
    const int ibin,
    const std::string name,
    std::vector<std::string> paramNames,
    std::string refDir
  );
};

class CExponential : public CBackgroundModel
{
public:
  CExponential(RooRealVar &m, const Bool_t pass);
  ~CExponential();
  RooRealVar *t;
};

class CErfcExpo : public CBackgroundModel
{
public:
  CErfcExpo(RooRealVar &m, const Bool_t pass);
  ~CErfcExpo();
  RooRealVar *alfa, *beta, *gamma, *peak; 
};

class CErfcExpoFixed : public CBackgroundModel
{
public:
  CErfcExpoFixed(RooRealVar &m, const Bool_t pass, const int ibin, const std::string name, std::string refDir );
  ~CErfcExpoFixed();
  RooRealVar *alfa, *beta, *gamma, *peak; 
};

class CDoubleExp : public CBackgroundModel
{
public:
  CDoubleExp(RooRealVar &m, const Bool_t pass);
  ~CDoubleExp();
  RooExponential *exp1, *exp2;
  RooRealVar *t1, *t2, *frac;
};

class CLinearExp : public CBackgroundModel
{
public:
  CLinearExp(RooRealVar &m, const Bool_t pass);
  ~CLinearExp();
  RooRealVar *a, *t;
};

class CQuadraticExp : public CBackgroundModel
{
public:
  CQuadraticExp(RooRealVar &m, const Bool_t pass);
  ~CQuadraticExp();
  RooRealVar *a1, *a2, *t;
};

class CBkgMCTemplateConvGaussian : public CBackgroundModel
{
public:
  CBkgMCTemplateConvGaussian(RooRealVar &m, TH1D* hist, const Bool_t pass, RooRealVar *sigma0=0, int intOrder=1);
  ~CBkgMCTemplateConvGaussian();
  RooRealVar  *mean, *sigma;
  RooGaussian *gaus;
  TH1D        *inHist;
  RooDataHist *dataHist;
  RooHistPdf  *histPdf;
};

class CBkgMC2TemplateConvGaussian : public CBackgroundModel
{
public:
  // add two templates with a floating fraction that varies by 20%
  // currently smearing is correlated but we might want to change this?
  CBkgMC2TemplateConvGaussian(RooRealVar &m, TH1D* hist0, TH1D* hist1, const Bool_t pass, RooRealVar *frac0=0, RooRealVar *sigma0=0, int intOrder=1, bool doConv=false);
  ~CBkgMC2TemplateConvGaussian();
  RooRealVar  *mean, *sigma;
  RooGaussian *gaus;
  TH1D        *inHist0, *inHist1;
  RooDataHist *dataHist0, *dataHist1;
  RooHistPdf  *histPdf0, *histPdf1;
  RooAddPdf   *sumPdf;
  RooRealVar *frac;
};


class CBkgMCDatasetConvGaussian : public CBackgroundModel
{
public:
  CBkgMCDatasetConvGaussian(RooRealVar &m, TTree* tree, const Bool_t pass, RooRealVar *sigma0=0);
  ~CBkgMCDatasetConvGaussian();
  RooRealVar  *mean, *sigma;
  RooGaussian *gaus;
  TTree       *inTree;
  RooDataSet  *dataSet;
  RooKeysPdf  *keysPdf;
};

class CBkgNGauss : public CBackgroundModel
{
  // kill me now
  public:
    CBkgNGauss(RooRealVar &m, const Bool_t pass, int nPeaks, double mu0_init, double mu1_init=0, double mu2_init=0);
    ~CBkgNGauss();
    RooRealVar *mu0, *mu1=0, *mu2=0;
    RooRealVar *sigma0, *sigma1=0, *sigma2=0;
    RooRealVar *frac0, *frac1=0, *frac2=0;
    RooBreitWigner *gaus0, *gaus1=0, *gaus2=0;
};

class CGauss : public CBackgroundModel
{
  public:
    CGauss(RooRealVar &m, const Bool_t pass, double mu0_init);
    ~CGauss();
    RooRealVar *mu0;
    RooRealVar *sigma0;
};

class CGaussExp : public CBackgroundModel
{
  // gaussian plus exponential
  public:
    CGaussExp(RooRealVar &m, const Bool_t pass);
    ~CGaussExp();
    RooRealVar *mu, *sigma, *alpha, *frac;
    RooGaussian *gauss;
    RooExponential *exp;
};

//--------------------------------------------------------------------------------------------------
CGaussExp::CGaussExp(RooRealVar &m, const Bool_t pass)
{
  char name[10];
  if(pass) sprintf(name,"%s","BkgPass");
  else     sprintf(name,"%s","BkgFail");
  
  char vname[50];

  if (pass) {  
    sprintf(vname,"mu%s",name);
mu = new RooRealVar(vname,vname,0.901368,0.901368,0.901368);
//    mu = new RooRealVar(vname,vname,177.201355,141.761084,212.641626);
    sprintf(vname,"sigma%s",name);
sigma = new RooRealVar(vname,vname,-16.079693,-16.079693,-16.079693);
//    sigma = new RooRealVar(vname,vname,-16.079693,-12.863755,-19.295632);
    sprintf(vname,"alpha%s",name);
alpha = new RooRealVar(vname,vname,-0.009624,-0.009624,-0.009624);
//    alpha = new RooRealVar(vname,vname,-0.009624,-0.007699,-0.011549);
    sprintf(vname,"frac%s",name);
    frac = new RooRealVar(vname,vname,0.848767,0.848767,0.848767); 
  } else {
    sprintf(vname,"mu%s",name);
//    mu = new RooRealVar(vname,vname,79.596417,63.677134,95.515701);
    mu = new RooRealVar(vname,vname,79.596417,79.596417,79.596417);
    sprintf(vname,"sigma%s",name);
//    sigma = new RooRealVar(vname,vname,9.078738,7.262990,10.894485);
    sigma = new RooRealVar(vname,vname,9.078738,9.078738,9.078738);
    sprintf(vname,"alpha%s",name);
//    alpha = new RooRealVar(vname,vname,-0.012407,-0.009926,-0.014889);
    alpha = new RooRealVar(vname,vname,-0.012407,-0.012407,-0.012407);
    sprintf(vname,"frac%s",name);
    frac = new RooRealVar(vname,vname,0.766902,0.766902,0.766902);
  }


  sprintf(vname,"gauss%s",name);
  gauss = new RooGaussian(vname,vname,m,*mu,*sigma);

  sprintf(vname,"exp%s",name);
  exp = new RooExponential(vname,vname,m,*alpha);
  
  sprintf(vname,"background%s",name);
  model = new RooAddPdf(vname,vname,RooArgList(*gauss,*exp),RooArgList(*frac));
  
}

CGaussExp::~CGaussExp()
{
  delete mu;     mu=0;
  delete sigma;  sigma=0;
  delete alpha;     alpha=0;
  delete frac;     frac=0;
  delete exp;     exp=0;
  delete gauss;     gauss=0;
}

//--------------------------------------------------------------------------------------------------
CBkgNGauss::CBkgNGauss(RooRealVar &m, const Bool_t pass, int nPeaks, double mu0_init, double mu1_init, double mu2_init)
{
  char name[10];
  if(pass) sprintf(name,"%s","BkgPass");
  else     sprintf(name,"%s","BkgFail");
  
  char vname[50];
  sprintf(vname,"mu0%s",name);
  mu0 = new RooRealVar(vname,vname,mu0_init,mu0_init-50, mu0_init+50);
  sprintf(vname,"sigma0%s",name);
  sigma0 = new RooRealVar(vname,vname,100,10,200);    
  sprintf(vname,"frac0%s",name);     
  frac0 = new RooRealVar(vname,vname,.2,0,1);
//  if (nPeaks==1)
//    sprintf(vname,"background%s",name);
//  else
    sprintf(vname,"gaus0%s",name);
  gaus0 = new RooBreitWigner(vname,vname,m,*mu0,*sigma0);
 
  if (nPeaks>1) { 
    sprintf(vname,"mu1%s",name);
    mu1 = new RooRealVar(vname,vname,mu1_init,mu1_init-50, mu1_init+50);
    sprintf(vname,"sigma1%s",name);
    sigma1 = new RooRealVar(vname,vname,50,10,100);    
    sprintf(vname,"frac1%s",name);     
    frac1 = new RooRealVar(vname,vname,.3,0,.5);
    sprintf(vname,"gaus1%s",name);
    gaus1 = new RooBreitWigner(vname,vname,m,*mu1,*sigma1);
    if (nPeaks>2) {
      sprintf(vname,"mu2%s",name);
      mu2 = new RooRealVar(vname,vname,mu2_init,mu2_init-50, mu2_init+50);
      sprintf(vname,"sigma2%s",name);
      sigma2 = new RooRealVar(vname,vname,10,1,100);    
      sprintf(vname,"frac2%s",name);     
      frac2 = new RooRealVar(vname,vname,.3,0,1);
      sprintf(vname,"gaus2%s",name);
      gaus2 = new RooBreitWigner(vname,vname,m,*mu2,*sigma2);
    }
  }

  sprintf(vname,"alpha%s",name);
  RooRealVar *alpha = new RooRealVar(vname,vname,-1.,-10.,0.);

  sprintf(vname,"exp%s",name);
  RooExponential *exp = new RooExponential(vname,vname,m,*alpha);

  sprintf(vname,"background%s",name);
  if (nPeaks==3)
    model = new RooAddPdf(vname,vname,RooArgList(*gaus0,*gaus1,*gaus2),RooArgList(*frac0,*frac1));
  else if (nPeaks==2)
    model = new RooAddPdf(vname,vname,RooArgList(*gaus0,*gaus1),RooArgList(*frac0));
  else if (nPeaks==1) {
    model = new RooAddPdf(vname,vname,RooArgList(*gaus0,*exp),RooArgList(*frac0));
//    model = gaus0;
//    gaus0=0; // so we don't try to double-free memory
  }
  
}

CBkgNGauss::~CBkgNGauss()
{
  delete mu0;     mu0=0;
  delete mu1;     mu1=0;
  delete mu2;     mu2=0;
  delete sigma0;  sigma0=0;
  delete sigma1;  sigma1=0;
  delete sigma2;  sigma2=0;
  delete frac0;   frac0=0;
  delete frac1;   frac1=0;
  delete frac2;   frac2=0;
  delete gaus0;   gaus0=0;
  delete gaus1;   gaus1=0;
  delete gaus2;   gaus2=0;
}

//--------------------------------------------------------------------------------------------------
CBkgMC2TemplateConvGaussian::CBkgMC2TemplateConvGaussian(RooRealVar &m, TH1D* hist0, TH1D* hist1, const Bool_t pass, RooRealVar *frac0, RooRealVar *sigma0, int intOrder, bool doConv)
{  
  char name[10];
  if(pass) sprintf(name,"%s","BkgPass");
  else     sprintf(name,"%s","BkgFail");
  
  char vname[50];  
  
  if (doConv) {
    sprintf(vname,"mean%s",name);  mean  = new RooRealVar(vname,vname,0.,-5.,5.);
    if(sigma0) { sigma = sigma0; }
    else       { sprintf(vname,"sigma%s",name); sigma = new RooRealVar(vname,vname,.5,.5,10.); }
  } else {
    sprintf(vname,"mean%s",name);  mean  = new RooRealVar(vname,vname,0.,-0.,0.);
    if(sigma0) { sigma = sigma0; }
    else       { sprintf(vname,"sigma%s",name); sigma = new RooRealVar(vname,vname,.5,.5,.5); }
  }
  sprintf(vname,"gaus%s",name);                 gaus  = new RooGaussian(vname,vname,m,*mean,*sigma);

  sprintf(vname,"inHist0_%s",hist0->GetName()); inHist0 = (TH1D*)hist0->Clone(vname);
  sprintf(vname,"dataHist0%s",name);            dataHist0 = new RooDataHist(vname,vname,RooArgSet(m),inHist0);
  sprintf(vname,"histPdf0%s",name);             histPdf0 = new RooHistPdf(vname,vname,m,*dataHist0,intOrder);

  sprintf(vname,"inHist1_%s",hist1->GetName()); inHist1 = (TH1D*)hist1->Clone(vname);
  sprintf(vname,"dataHist1%s",name);            dataHist1 = new RooDataHist(vname,vname,RooArgSet(m),inHist1);
  sprintf(vname,"histPdf1%s",name);             histPdf1 = new RooHistPdf(vname,vname,m,*dataHist1,intOrder);

  if (frac0) frac = frac0;
  else {
    double frac_cent = inHist0->Integral()/(inHist0->Integral()+inHist1->Integral());
    // frac_cent = 0.35;
    sprintf(vname,"frac%s",name);               frac = new RooRealVar(vname,vname,frac_cent,frac_cent*0.8,std::min(1.,frac_cent*1.2));
    //sprintf(vname,"frac%s",name);                 frac = new RooRealVar(vname,vname,frac_cent,frac_cent,frac_cent);
  }

  sprintf(vname,"sumTemplate%s",name);          sumPdf = new RooAddPdf(vname,vname,RooArgList(*histPdf0,*histPdf1),RooArgList(*frac));

  sprintf(vname,"background%s",name);           model = new RooFFTConvPdf(vname,vname,m,*sumPdf,*gaus);
}

CBkgMC2TemplateConvGaussian::~CBkgMC2TemplateConvGaussian()
{
  delete mean;     mean=0;
  //delete sigma;    sigma=0;
  delete gaus;     gaus=0;
  delete inHist0;   inHist0=0;
  delete dataHist0; dataHist0=0;
  delete histPdf0;  histPdf0=0;
  delete inHist1;   inHist1=0;
  delete dataHist1; dataHist1=0;
  delete histPdf1;  histPdf1=0;
  //delete frac;      frac=0;
  delete sumPdf;    sumPdf=0;
}
//--------------------------------------------------------------------------------------------------
CBkgMCTemplateConvGaussian::CBkgMCTemplateConvGaussian(RooRealVar &m, TH1D* hist, const Bool_t pass, RooRealVar *sigma0, int intOrder)
{  
  char name[10];
  if(pass) sprintf(name,"%s","BkgPass");
  else     sprintf(name,"%s","BkgFail");
  
  char vname[50];  
  
  sprintf(vname,"mean%s",name);  mean  = new RooRealVar(vname,vname,0.,-0.,0.);
  if(sigma0) { sigma = sigma0; }
  else       { sprintf(vname,"sigma%s",name); sigma = new RooRealVar(vname,vname,1.,.5,10.); }
  sprintf(vname,"gaus%s",name);  gaus  = new RooGaussian(vname,vname,m,*mean,*sigma);

  sprintf(vname,"inHist_%s",hist->GetName());
  inHist = (TH1D*)hist->Clone(vname);
  
  sprintf(vname,"dataHist%s",name); dataHist = new RooDataHist(vname,vname,RooArgSet(m),inHist);
  sprintf(vname,"histPdf%s",name);  histPdf  = new RooHistPdf(vname,vname,m,*dataHist,intOrder);
  sprintf(vname,"background%s",name);   model    = new RooFFTConvPdf(vname,vname,m,*histPdf,*gaus);
}

CBkgMCTemplateConvGaussian::~CBkgMCTemplateConvGaussian()
{
  delete mean;     mean=0;
  //delete sigma;    sigma=0;
  delete gaus;     gaus=0;
  delete inHist;   inHist=0;
  delete dataHist; dataHist=0;
  delete histPdf;  histPdf=0;
}
//--------------------------------------------------------------------------------------------------
CBkgMCDatasetConvGaussian::CBkgMCDatasetConvGaussian(RooRealVar &m, TTree* tree, const Bool_t pass, RooRealVar *sigma0)
{  
  char name[10];
  if(pass) sprintf(name,"%s","BkgPass");
  else     sprintf(name,"%s","BkgFail");
  
  char vname[50];  

  if(pass) {
    sprintf(vname,"mean%s",name);  mean  = new RooRealVar(vname,vname,0,-10,10);
    if(sigma0) { sigma = sigma0; }
    else       { sprintf(vname,"sigma%s",name); sigma = new RooRealVar(vname,vname,2,0,5); }
    sprintf(vname,"gaus%s",name);  gaus  = new RooGaussian(vname,vname,m,*mean,*sigma);
  } else {
    sprintf(vname,"mean%s",name);  mean  = new RooRealVar(vname,vname,0,-10,10);
    if(sigma0) { sigma = sigma0; }
    else       { sprintf(vname,"sigma%s",name); sigma = new RooRealVar(vname,vname,2,0,5); }
    sprintf(vname,"gaus%s",name);  gaus  = new RooGaussian(vname,vname,m,*mean,*sigma);
  }
  
  sprintf(vname,"inTree_%s",tree->GetName());
  inTree = (TTree*)tree->Clone(vname);
  
  sprintf(vname,"dataSet%s",name); dataSet = new RooDataSet(vname,vname,inTree,RooArgSet(m));
  sprintf(vname,"keysPdf%s",name); keysPdf = new RooKeysPdf(vname,vname,m,*dataSet,RooKeysPdf::NoMirror,1);
  sprintf(vname,"background%s",name);  model   = new RooFFTConvPdf(vname,vname,m,*keysPdf,*gaus);
}

CBkgMCDatasetConvGaussian::~CBkgMCDatasetConvGaussian()
{
  delete mean;    mean=0;
  delete sigma;   sigma=0;
  delete gaus;    gaus=0;
  delete inTree;  inTree=0;
  delete dataSet; dataSet=0;
  delete keysPdf; keysPdf=0;
}

//--------------------------------------------------------------------------------------------------
CExponential::CExponential(RooRealVar &m, const Bool_t pass)
{
  char name[10];
  if(pass) sprintf(name,"%s","BkgPass");
  else     sprintf(name,"%s","BkgFail");
  
  char vname[50];
  
  sprintf(vname,"t%s",name);
  if(pass)
    t = new RooRealVar(vname,vname,-0.04,-1.,-0.05);
  else
    t = new RooRealVar(vname,vname,-0.04,-1.,-0.05);
      
  sprintf(vname,"background%s",name);
  model = new RooExponential(vname,vname,m,*t);
}

CExponential::~CExponential()
{
  delete t;
  t=0;
}

//--------------------------------------------------------------------------------------------------
CErfcExpo::CErfcExpo(RooRealVar &m, const Bool_t pass)
{
  char name[10];
  if(pass) sprintf(name,"%s","BkgPass");
  else     sprintf(name,"%s","BkgFail");
  
  char vname[50];

  if(pass) {
    sprintf(vname,"alfa%s",name);  alfa  = new RooRealVar(vname,vname,50,5,200);
    sprintf(vname,"beta%s",name);  beta  = new RooRealVar(vname,vname,0.05,0,0.2);
    sprintf(vname,"gamma%s",name); gamma = new RooRealVar(vname,vname,0.1,0,1);
  } else {
    sprintf(vname,"alfa%s",name);  alfa  = new RooRealVar(vname,vname,50,5,200);
    sprintf(vname,"beta%s",name);  beta  = new RooRealVar(vname,vname,0.05,0,0.2);
    sprintf(vname,"gamma%s",name); gamma = new RooRealVar(vname,vname,0.1,0,1);
  }  
  
  sprintf(vname,"peak%s",name);  
  peak = new RooRealVar(vname,vname,91.1876,85,97); 
  peak->setVal(91.1876);
  peak->setConstant(kTRUE);  
  
  sprintf(vname,"background%s",name);
  model = new RooCMSShape(vname,vname,m,*alfa,*beta,*gamma,*peak);
}

CErfcExpo::~CErfcExpo()
{
  delete alfa;  alfa=0;
  delete beta;  beta=0;
  delete gamma; gamma=0;
  delete peak;  peak=0;
}

//--------------------------------------------------------------------------------------------------
CErfcExpoFixed::CErfcExpoFixed(RooRealVar &m, const Bool_t pass, const int ibin, const std::string fitname, std::string refDir)
{
  char name[10];
  if(pass) sprintf(name,"%s","BkgPass");
  else     sprintf(name,"%s","BkgFail");
  
  char vname[50];
  std::vector<std::string> paramNames;
  if(pass) {
    paramNames.push_back("alfaPass");
    paramNames.push_back("betaPass");
    paramNames.push_back("gammaPass");
  } else {
    paramNames.push_back("alfaFail");
    paramNames.push_back("betaFail");
    paramNames.push_back("gammaFail");
  }
  std::vector<double> params = readBkgParams(ibin, fitname, paramNames, refDir);
  sprintf(vname,"alfa%s",name);  alfa  = new RooRealVar(vname,vname,params[0]); alfa->setConstant(kTRUE);
  sprintf(vname,"beta%s",name);  beta  = new RooRealVar(vname,vname,params[1]); beta->setConstant(kTRUE);
  sprintf(vname,"gamma%s",name); gamma = new RooRealVar(vname,vname,params[2]); gamma->setConstant(kTRUE);
  
  sprintf(vname,"peak%s",name);  
  peak = new RooRealVar(vname,vname,91.1876,85,97); 
  peak->setVal(91.1876);
  peak->setConstant(kTRUE);  
  
  sprintf(vname,"background%s",name);
  model = new RooCMSShape(vname,vname,m,*alfa,*beta,*gamma,*peak);
}

CErfcExpoFixed::~CErfcExpoFixed()
{
  delete alfa;  alfa=0;
  delete beta;  beta=0;
  delete gamma; gamma=0;
  delete peak;  peak=0;
}

//--------------------------------------------------------------------------------------------------
CDoubleExp::CDoubleExp(RooRealVar &m, const Bool_t pass)
{
  char name[10];
  if(pass) sprintf(name,"%s","BkgPass");
  else     sprintf(name,"%s","BkgFail");
  
  char vname[50];
 
  if(pass) {
    sprintf(vname,"t1%s",name);   t1   = new RooRealVar(vname,vname,-0.20,-1.,0.);
    sprintf(vname,"t2%s",name);   t2   = new RooRealVar(vname,vname,-0.05,-1.,0.);
    sprintf(vname,"frac%s",name); frac = new RooRealVar(vname,vname, 0.50, 0.,1.);
  } else {
    sprintf(vname,"t1%s",name);   t1   = new RooRealVar(vname,vname,-0.20,-1.,0.);
    sprintf(vname,"t2%s",name);   t2   = new RooRealVar(vname,vname,-0.05,-1.,0.);
    sprintf(vname,"frac%s",name); frac = new RooRealVar(vname,vname, 0.50, 0.,1.);
  }
    
  sprintf(vname,"exp1%s",name);
  exp1 = new RooExponential(vname,vname,m,*t1);
  sprintf(vname,"exp2%s",name);
  exp2 = new RooExponential(vname,vname,m,*t2);
  sprintf(vname,"background%s",name);
  model = new RooAddPdf(vname,vname,RooArgList(*exp1,*exp2),RooArgList(*frac));
}

CDoubleExp::~CDoubleExp()
{
  delete exp1; exp1=0;
  delete exp2; exp2=0;
  delete t1;   t1=0;
  delete t2;   t2=0;
  delete frac; frac=0;
}

//--------------------------------------------------------------------------------------------------
CLinearExp::CLinearExp(RooRealVar &m, const Bool_t pass)
{
  char name[10];
  if(pass) sprintf(name,"%s","BkgPass");
  else     sprintf(name,"%s","BkgFail");
  
  char aname[50];
  sprintf(aname,"a%s",name);
  a = new RooRealVar(aname,aname,-0,-10.,10.);
  //a->setConstant(kTRUE);
  
  char tname[50];
  sprintf(tname,"t%s",name);
  t = new RooRealVar(tname,tname,-1e-6,-10.,0.);
  //t->setConstant(kTRUE); 
  
  char formula[200];
  sprintf(formula,"(1+%s*m)*exp(%s*m)",aname,tname);
 
  char vname[50]; sprintf(vname,"background%s",name);
  model = new RooGenericPdf(vname,vname,formula,RooArgList(m,*a,*t));
}

CLinearExp::~CLinearExp()
{
  delete a; a=0;
  delete t; t=0;
}

//--------------------------------------------------------------------------------------------------
CQuadraticExp::CQuadraticExp(RooRealVar &m, const Bool_t pass)
{
  char name[10];
  if(pass) sprintf(name,"%s","BkgPass");
  else     sprintf(name,"%s","BkgFail");

  char a1name[50]; 
  sprintf(a1name,"a1%s",name);
  a1 = new RooRealVar(a1name,a1name,0,-10,10.);
  //a1->setConstant(kTRUE);
  
  char a2name[50]; 
  sprintf(a2name,"a2%s",name);
  a2 = new RooRealVar(a2name,a2name,0.0,-10,10);
  //a2->setConstant(kTRUE);
  
  char tname[50];
  sprintf(tname,"t%s",name);
  t = new RooRealVar(tname,tname,-1e-6,-10.,0.); 
  //t->setConstant(kTRUE); 
  
  char formula[200];
  sprintf(formula,"(1+%s*m+%s*m*m)*exp(%s*m)",a1name,a2name,tname);
 
  char vname[50]; sprintf(vname,"background%s",name);
  model = new RooGenericPdf(vname,vname,formula,RooArgList(m,*a1,*a2,*t));
}

CQuadraticExp::~CQuadraticExp()
{
  delete a1; a1=0;
  delete a2; a2=0;
  delete t;  t=0;
}

std::vector<double> CBackgroundModel::readBkgParams(
  const int ibin,
  const std::string fitname,
  std::vector<std::string> paramNames,
  std::string refDir
) {
  std::vector<double> params;
  char rname[512];
  sprintf(
    rname,
    "%s/plots/fitres%s_%i.txt",
    refDir.c_str(),
    fitname.c_str(),
    ibin
  );
  ifstream rfile;
  for(unsigned int i = 0; i<paramNames.size(); i++) {
    rfile.open(rname);
    assert(rfile.is_open());
    std::string line;
    bool found_param = false;
    printf("Looking for parameter %s...\n", paramNames[i].c_str());
    while(getline(rfile,line)) {
      printf("%s\n", line.c_str());
      size_t found = line.find(paramNames[i]);
      if(found!=string::npos) {
        std::string varname, initval, finalval, pmstr, error, corr;
        std::stringstream ss(line);
        ss >> varname >> initval >> finalval >> pmstr >> error >> corr;
        params.push_back(atof(finalval.c_str()));
        found_param=true;
        break;
      }
    }
    assert(found_param);
    rfile.close();
  }
  return params;
}

